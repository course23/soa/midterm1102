/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.FoodKoala;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 按照提示完成下列函式，加入程式碼及合適的 annotation （50%）
 */
public class OrderController {
    // 用來顯示所有的 Order，利用 Order.items 可以抓到現有的 order
    public String listOrders(Model model){
        return "list";
    }
    
    // 用來轉址到新增 Order 的頁面，提示，可能會用到 FoodDatabase.getAllFoodNames()
    public String addFood(Model model){
        return "add";
    }
    
    //用來根據index刪除一個 Order
    public String deleteFood(int index){
        return "redirect:/";
    }
    
    // 用來新增一筆 Order,提示，會用到 Order.items
    public String addOrder(Order order){
        return "redirect:/";
    }
}
