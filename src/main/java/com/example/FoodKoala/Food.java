/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.FoodKoala;

import java.util.List;

/**
 *
 * @author lendle
 */
public class Food {
    private String name=null;
    private double unitPrice=0;
    
    public Food(String name, double unitPrice) {
        this.name=name;
        this.unitPrice=unitPrice;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
    
}
