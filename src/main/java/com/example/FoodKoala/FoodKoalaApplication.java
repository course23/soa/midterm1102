package com.example.FoodKoala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodKoalaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodKoalaApplication.class, args);
	}

}
