/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.FoodKoala;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lendle
 */
public class Order {
    public static List<Order> items=new ArrayList<>();
    private String foodName=null;
    private int amount=-1;

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    
}
