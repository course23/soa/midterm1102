/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.FoodKoala;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lendle
 */
public class FoodDatabase {
    private static Map<String, Food> foods=Map.of(
            "漢堡", new Food("漢堡", 80),
            "可樂", new Food("可樂", 30),
            "薯條", new Food("薯條", 30),
            "炸雞", new Food("炸雞", 60),
            "玉米濃湯", new Food("玉米濃湯", 40));
    public static Food getFood(String name){
        return foods.get(name);
    }
    public static List<String> getAllFoodNames(){
        return new ArrayList<>(foods.keySet());
    }
}
